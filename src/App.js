import logo from './logo.svg';
import './App.css';
import SignInSide from './bankcomponents/LoginUI';
import Forget from './bankcomponents/Forget';
import Email from './bankcomponents/Email';
import React from "react";
import './Thestyle.css';
import Dashboard from "./bankcomponents/Dashboard";
import UserDashboard from './userDash/UserDashboard';
import Approval from './bankcomponents/Approval';
import PasswordChanged from './bankcomponents/PasswordChanged';
import PasswordReset from './bankcomponents/PasswordReset';
import DisplayJSON from './bankcomponents/SetJSON';
import AdminDashBoard from './userDash/AdminDashBoard';
import AdminDashBoard2 from './userDash/AdminDashBoard2';
import AdminDashBoard3 from './userDash/AdminDashBoard3';
import AdminDashBoard4 from './userDash/AdminDashBoard4';
import LandingPage from './homepage/LandingPage';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <div>

      <Router>
        <Switch>
          <Route path="/creditcard">
            <AdminDashBoard4 />
          </Route>
          <Route path="/approvestatus">
            <AdminDashBoard3 />
          </Route>
          <Route path="/customerdetails">
            <AdminDashBoard2 />
          </Route>
          <Route path="/admindashboard">
            <AdminDashBoard />
          </Route>
          <Route path="/passwordchanged">
            <PasswordChanged />
          </Route>
          <Route path="/resetpassword">
            <PasswordReset />
          </Route>
          <Route path="/adminappproval">
            <Approval />
          </Route>
          <Route path="/userdashboard">
            <UserDashboard />
          </Route>
          <Route path="/admindashboard">
            <Dashboard />
          </Route>
          <Route path="/forgetpassword">
            <Forget />
          </Route>
          <Route path="/emailsent">
            <Email />
          </Route>
          <Route path="/loginpage">
            <SignInSide />
            <DisplayJSON />
          </Route>
          <Route path="/">
            <LandingPage />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}


export default App;
