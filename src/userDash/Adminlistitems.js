import React from 'react';
import HomeIcon from '@material-ui/icons/Home';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import BarChartIcon from '@material-ui/icons/BarChart';
import LayersIcon from '@material-ui/icons/Layers';
import AssignmentIcon from '@material-ui/icons/Assignment';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';


const handleLogout = (e) => {
  window.setTimeout(function () {
    window.location.href = 'http://localhost:3001/';
  }, 200);
  console.log("logout")
}

const handleCustomerDetails = (e) =>{
  window.setTimeout(function () {
    window.location.href = 'http://localhost:3001/customerdetails';
  }, 200);

}

const overview = (e) =>{
  window.setTimeout(function () {
    window.location.href = 'http://localhost:3001/admindashboard';
  }, 200);

}


const approvalStatus = (e) =>{
  window.setTimeout(function () {
    window.location.href = 'http://localhost:3001/approvestatus';
  }, 500);

}

const creditcardstatus = (e) =>{
  window.setTimeout(function () {
    window.location.href = 'http://localhost:3001/creditcard';
  }, 500);
}




export const mainListItems = (
  <div>
    <ListItem button onClick={overview}>
      <ListItemIcon>
        <HomeIcon />
      </ListItemIcon  >
      <h3>Overview</h3>
    </ListItem>
    <ListItem button onClick={handleCustomerDetails}>
      <ListItemIcon>
        <ContactMailIcon />
      </ListItemIcon>
      <ListItemText primary="Customer Details" />
    </ListItem>
    <ListItem button onClick={approvalStatus}>
      <ListItemIcon>
        <PeopleIcon />
      </ListItemIcon>
      <ListItemText primary="Approval Details" />
    </ListItem>
    <ListItem button onClick={creditcardstatus}>
      <ListItemIcon>
        <CreditCardIcon />
      </ListItemIcon>
      <ListItemText primary="Credit Card Status" />
    </ListItem>
    <ListItem button onClick={handleLogout}>
      <ListItemIcon>
        <ExitToAppIcon />
      </ListItemIcon>
      <ListItemText primary="Logout" />
    </ListItem>
  </div>
);

export const secondaryListItems = (
  <div>
    <ListSubheader inset>Saved reports</ListSubheader>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Current month" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Last quarter" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Year-end sale" />
    </ListItem>
  </div>
);