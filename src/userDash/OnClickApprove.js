import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './UserTitle';
import { borders } from '@material-ui/system';
import emailjs from 'emailjs-com';
import{ init } from 'emailjs-com';
init("user_zUY9QbzZYe2yqzV8JB7zE");
 


// Generate Order Data
function createData(id, email, password) {
    return { id, email, password};
  }

const rows = [];



 function whichdatashouldbecreated(){

// add the customers only from the local storage
var number = 1;


while(true){
  if(localStorage.getItem("registereditem"+number) === null ){
    return;
  }else{

    if(!localStorage.getItem("registereditem"+number) === false){

    var registeredCustomer = localStorage.getItem("registereditem"+number);
    console.log(registeredCustomer);
    const customerObj = JSON.parse(registeredCustomer);


    var maparray = rows.map((row) => row.id);

    for(let j = 0; j< rows.length; j++){

      if(customerObj["id"] === maparray[j]){
        return;
      }
    }


    rows.push(createData(customerObj["id"], customerObj["email"], customerObj["password"]));
    number++;
  }

  }

}




}


const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));


function approveAccount(){
  

  // get the data from the customer id innerhtml. From Local Storage
  var approvedCustomerId = localStorage.getItem("ApprovedAccount");
  var approvedcustomerdata;

  // disable the approve  buttons
  document.getElementById(approvedCustomerId).disabled = true;



  // retrieve data from local storage
  for(var i = 1; i<= rows.length; i++){
     // get the i data
     approvedcustomerdata = localStorage.getItem("registereditem"+i);
     // parse the object
     const customerObj = JSON.parse(approvedcustomerdata);

     

    if(approvedCustomerId == customerObj["id"]){

      // disable deny button
      document.getElementById(customerObj["password"]).disabled = true;

      // call send email function
      sendEmail(customerObj["fullname"], customerObj["email"], "Registration Approved", "Congratulations, your account has been approved. You are now able to login into your account.");
      
      // remove from local storage since approved alr
      approvedcustomerdata = localStorage.getItem("registereditem"+i);
      console.log("registereditem"+i);
      localStorage.removeItem("registereditem"+i);

      var nextitem = i+1;

      if(!localStorage.getItem("registereditem"+nextitem)=== false){
        var thenextitem = localStorage.getItem("registereditem"+nextitem);
        localStorage.setItem("registereditem"+i, thenextitem);
        localStorage.removeItem("registereditem"+nextitem);
      }

    break;

  
    }
  }

  const customerObj = JSON.parse(approvedcustomerdata);

    var axios = require('axios');
    var data = JSON.stringify({
      "id": customerObj["id"],
      "fullname": customerObj["fullname"],
      "email": customerObj["email"],
      "password": customerObj["password"],
      "group": "customer"
    });

    var config = {
      method: 'post',
      url: 'http://localhost:3000/Users',
      headers: { 
        'Content-Type': 'application/json'
      },
      data : data
    };

    axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      console.log("Approval successful");
      document.getElementById(customerObj["email"]).innerHTML = "Approved";
      document.getElementById(customerObj["email"]).style.color = "green";
    })
    .catch(function (error) {
      console.log(error);
      document.getElementById(customerObj["email"]).innerHTML = "Not Approved";
      document.getElementById(customerObj["email"]).style.color = "red";
    });
}



function denyAccount() {

  // get the data from the customer id innerhtml. From Local Storage
  var deniedCustomerId = localStorage.getItem("DeniedAccount");
  var deniedcustomerdata;

  // disable the approve button
  document.getElementById(deniedCustomerId).disabled = true;

  // retrieve data from local storage
  for(var i = 1; i<= rows.length; i++){
     
     // get the i data
     deniedcustomerdata = localStorage.getItem("registereditem"+i);
     // parse the object
     const customerObj = JSON.parse(deniedcustomerdata);
     console.log(deniedCustomerId);
     console.log(customerObj["id"]);
    if(deniedCustomerId == customerObj["id"]){

      // disable deny button
     document.getElementById(customerObj["password"]).disabled = true;

      // call send email function
      sendEmail(customerObj["fullname"], customerObj["email"], "Registration Denied", "We regret to inform you that your registration is denied. Please register again. Thank you.");

      // remove from local storage
      deniedcustomerdata = localStorage.getItem("registereditem"+i);
      console.log("registereditem"+i);
      localStorage.removeItem("registereditem"+i);

      var nextitem = i+1;

      if(!localStorage.getItem("registereditem"+nextitem)=== false){
        var thenextitem = localStorage.getItem("registereditem"+nextitem);
        localStorage.setItem("registereditem"+i, thenextitem);
        localStorage.removeItem("registereditem"+nextitem);
      }
      break;
    }
  }

  const customerObj = JSON.parse(deniedcustomerdata);

  document.getElementById(customerObj["email"]).innerHTML = "Not Approved";
  document.getElementById(customerObj["email"]).style.color = "red";


}


function sendEmail(thefullname, theemail, theregistration, themessage) {
  
  var templateParams = {
    name: thefullname,
    registration: theregistration,
    message: themessage,
    email: theemail
  };

  emailjs.send('service_0vws2zk', 'template_5lh4e4e', templateParams)
    
    .then((result) => {
        console.log(result.text);
   
        
    }, (error) => {
        console.log(error.text);
    });

    
}


export default function OnClickApprovalDetails() {
  const classes = useStyles();
  whichdatashouldbecreated();

  return (

    <React.Fragment>
 
      
      <Title>Customer Account Approval Status</Title>
      
      <Table size="small">
        <TableHead>
        <TableRow>
            <TableCell><b>Customer ID</b></TableCell>
            <TableCell><b>Email</b></TableCell>
            <TableCell><b>Approve/Deny Account</b></TableCell>
            <TableCell></TableCell>
            

          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            
            <TableRow key={row.id}>
              <TableCell >{row.id}</TableCell>
              <TableCell>{row.email}</TableCell>
              <TableCell ><button class="approve" id={row.id} border={1} onClick = {() => {localStorage.setItem("ApprovedAccount", row.id);  approveAccount(); }}>Approve</button>  <button class="deny" id={row.password} border={1} onClick = {() => {localStorage.setItem("DeniedAccount", row.id); denyAccount();}}>Deny</button> </TableCell>
              <TableCell ><div id={row.email}></div></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div className={classes.seeMore}>
        {/*<Link color="primary" href="#" onClick={preventDefault}>
          See more orders
          </Link>*/}
      </div>
    </React.Fragment>
  );
}