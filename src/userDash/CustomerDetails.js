import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './UserTitle';
import data from "../bankcomponents/users"; 


// Generate Order Data
function createData(id, accountstatus, fullname, email, password, group) {
    return { id, accountstatus,  fullname, email, password, group};
  }

const fullname = data.Users.map((account) => account.fullname);
const email = data.Users.map((account) => account.email);
const password = data.Users.map((account) => account.password);
const theids = data.Users.map((account) => account.id);
const group = data.Users.map((account) => account.group);

 const rows = [];

function whichdatashouldbecreated(){

    // add the customers from the JSON file (Registered customer)
    for(let i = 0; i < email.length; i++){
    if(group[i] === "admin"){
      continue;

    }
    
    else{

      var maparray = rows.map((row) => row.id);

      for(let j = 0; j< email.length; j++){

        if(theids[i] === maparray[j]){
          return;
        }
      }

      rows.push(createData(theids[i], "Active", fullname[i], email[i], password[i], group[i]));

    }

  }

  // add the customers from the local storage
  var number = 1;

  while(true){
    if(localStorage.getItem("registereditem"+number) === null){
      break;
    }else{

      var registeredCustomer = localStorage.getItem("registereditem"+number);
      console.log(registeredCustomer);
      const customerObj = JSON.parse(registeredCustomer);
      rows.push(createData(customerObj["id"], "Pending", customerObj["fullname"], customerObj["email"], customerObj["password"], customerObj["group"]));
      number++;

    }

  }
  



}
  


function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function CustomerDetails() {
  const classes = useStyles();
  whichdatashouldbecreated();
  

 
  
  return (
    <React.Fragment>

    
      <Title>Customer Details</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell><b>Customer ID</b></TableCell>
            <TableCell><b>Account Status</b></TableCell>
            <TableCell><b>Name</b></TableCell>
            <TableCell><b>Email</b></TableCell>
            <TableCell><b>Password</b></TableCell>
            <TableCell ><b>Group</b></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.id}</TableCell>
              <TableCell>{row.accountstatus}</TableCell>
              <TableCell>{row.fullname}</TableCell>
              <TableCell>{row.email}</TableCell>
              <TableCell>{row.password}</TableCell>
              <TableCell >{row.group}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div className={classes.seeMore}>
        {/*<Link color="primary" href="#" onClick={preventDefault}>
          See more orders
          </Link>*/}
      </div>
    </React.Fragment>
  );
}