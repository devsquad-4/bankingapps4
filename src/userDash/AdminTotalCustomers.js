import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from './UserTitle';
import data from "../bankcomponents/users"; 



function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});

export default function TotalCustomers() {
  // the styles
  const classes = useStyles();

  // get how many current customers
  const users = data.Users;
  console.log(users);
  const howmanycustomers = users.length-1;

  
  return (
    <React.Fragment>
      <Title>Total Customers</Title>
      <Typography component="p" variant="h4">
      {howmanycustomers}
      </Typography>
      <div>
       
      </div>
    </React.Fragment>
  );
}