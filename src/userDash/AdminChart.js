import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import { LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts';
import Title from './UserTitle';
import Typography from '@material-ui/core/Typography';

// Generate Sales Data
function createData(time, amount) {
  return { time, amount };
}

const data = [
  createData('00:00', 0),
  createData('03:00', 300),
  createData('06:00', 600),
  createData('09:00', 800),
  createData('12:00', 1500),
  createData('15:00', 2000),
  createData('18:00', 2400),
  createData('21:00', 2400),
  createData('24:00', undefined),
];

export default function AdminChart() {
  const theme = useTheme();

  // get how many pending registration customer
  var i = 1;
  while(true){
      if(localStorage.getItem("registereditem" +i) === null){
        i--;
        break;
      }else{
          i++;
      }
  }

  return (
    <React.Fragment>
      <Title id="customerpending">Pending Customer Account Status</Title>
      <Typography component="p" variant="h4">
       {i}
      </Typography>
      
      
    </React.Fragment>
  );
}