
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';



function Removebackground(){
    document.body.style.background = "url() 0% fixed";
}

const useStyles = makeStyles((theme) => ({

    avatar: {
      margin: theme.spacing(25,84),
      backgroundColor: theme.palette.secondary.main,
      position: 'absolute',
      left: 0,
      right: 0,
      height: 200,
      width: 200,
   
    },

    passwordreset:{
        margin: theme.spacing(50,80.5),
        fontSize: "20px",
        color: 'black',
        position: 'absolute',
        left: 0,
        right: 0,
       

    },


    returntologinpage:{
        margin: theme.spacing(51,78),
        color: 'black',
        position: 'absolute',
        left: 0,
        right: 0,
    },

    icon:{
        height: 170,
        width: 170,
    }
    
   
 
  }));

export default function PasswordChanged(){
    const classes = useStyles();
    <Removebackground />

    return(
 
        <body id="passwordchangedcontainer">
        <Avatar className={classes.avatar} >
            <VpnKeyIcon className={classes.icon} />
        </Avatar>
        <br></br>
        <span className={classes.passwordreset}><b>Password has been Reset</b></span> 
        <br></br>

        <span className={classes.returntologinpage}>Click <Link href="/">Login Page</Link> to return to login page</span>
        </body>
    );
}