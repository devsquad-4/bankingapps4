import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import CreateIcon from '@material-ui/icons/Create';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useEffect, useState } from "react";
import users from "./users";
import { USERWHITESPACABLE_TYPES } from '@babel/types';
import Dashboard from "./Dashboard";
import { Event, Gradient } from '@material-ui/icons';



function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}




function Changetofaint() {

  const classes = useStyles();

  var [userChoice, setChoice] = useState("Sign In")

  const handleRegister = (e) => {
    e.preventDefault();
    const theids = users.Users.map((account) => account.id);
    var nextid =  Array.from(theids.values()).pop();
    var integernewid = JSON.parse(nextid);
    var newid = integernewid + 1;
  
  
    // get the data from the input field
    var registerfullname = document.getElementById("newfullname").value;
    var registeremail = document.getElementById("newemailaddress").value;
    var newregisteredpassword = document.getElementById("registerpassword").value;
    var retypethepassword = document.getElementById("retyperegisterpassword").value;
    var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    var validPasswordRegex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/;

    if(registerfullname === "" || registeremail === "" || newregisteredpassword === "" || retypethepassword === "" ){
      document.getElementById("registrationmessage").innerHTML = "Registration Unsuccessful. Please fill up all the fields";
      document.getElementById("registrationmessage").style.color = "red";
      return;
    }

    else if(registerfullname === "Nil"){
      return;
    }

    // email address doesnt meet the required format
   else if(!registeremail.match(validRegex)){
      document.getElementById("registrationmessage").innerHTML = "Invalid Email Address. Please type again";
      document.getElementById("newemailaddress").value = "";
      document.getElementById("registrationmessage").style.color = "red";

    }

    // password doesnt not meet the required format
    else if(!newregisteredpassword.match(validPasswordRegex)){
      document.getElementById("registrationmessage").innerHTML = "Password must be of length 6, contains a number, and a symbol";
      document.getElementById("registerpassword").value = "";
      document.getElementById("retyperegisterpassword").value = "";
      document.getElementById("registrationmessage").style.color = "red";

    // password doesnt match the retyped password  
    } else if(newregisteredpassword !== retypethepassword){
      document.getElementById("registrationmessage").innerHTML = "Passwords do not match. Please type again";
      document.getElementById("registerpassword").value = "";
      document.getElementById("retyperegisterpassword").value = "";
      document.getElementById("registrationmessage").style.color = "red";
    }


    
    
    else{

      // check if the user email exists alr
      const theemails = users.Users.map((account) => account.email);
      

      for(var i = 0; i< theemails.length; i++){

        if(theemails[i] === document.getElementById("newemailaddress").value){
          document.getElementById("registrationmessage").innerHTML = "This Email Address is already in use. Please type another email address.";
          document.getElementById("newemailaddress").value = "";
          document.getElementById("registrationmessage").style.color = "red";
          return;
        }

      }

 

      // check if there are local storage items that exists
      var i = 1;

      while(true){
      if(localStorage.getItem("registereditem"+i) === null){
        // place the attributes into a JSON format
        var registereditem = JSON.stringify({
          "id": newid,
          "fullname": registerfullname,
          "email": registeremail,
          "password": newregisteredpassword,
          "group": "customer",
          "accountStatus": "inactive"
        });

        // store the data in local storage
        localStorage.setItem("registereditem"+i, registereditem);

        // notify user that registration completed successfully
        console.log("Register pending for approval");
        document.getElementById("registrationmessage").innerHTML = "Registration Pending for Approval";
        document.getElementById("registrationmessage").style.color = "green";
        
        break;
      }else{
        // check if the id is similar. if yes then change
        let temp = JSON.parse(localStorage.getItem("registereditem"+i));
        newid = temp.id + 1;
        i++;
      }

    }

      
    }

  };


  // what is the use of useEffect???
  useEffect(() => {

    document.title = "Optimum Digibank";
    console.log("Use effect ran");
    console.log(userChoice);

    if (userChoice === "SignIn") {
      document.getElementById("theregister").style.color = "grey";
      document.getElementById("thesignin").style.color = "black";
      var myForm = document.getElementById("theform");
      document.getElementById("theform").innerHTML = "<p class=" + '"title"' + ">Log in</p>" +
        "<input id=" +'"id1"' +"type=" + '"text"' + "placeholder=" + '"Email"' + "autofocus/>" +
        "<span id=" +'"emailError"' +"></span>"+
        "<input id=" +'"id2"' +" type=" + '"password"' + "placeholder=" + '"Password"' +  " />" +
        "<span id=" +'"emailError2"' +"></span>" +
        "<br/>"+
        "<br/>"+
        "<a href=" + '"/forgetpassword"' + ">Forgot your password?</a>" +
        "<button>" +
        "<span class=" + '"state"' + "><b>Log in</b></span>" +
        "</button> "+
        "<br></br>"+
        "<br></br>"+
        "<b><span id=" +'"loginmessage"' +"></span></b>"+
        "<input id=" +'"newfullname"' + "type=" +'"hidden"' + "value=" +'"Nil"' +" autofocus />"+
        "<input id=" +'"newemailaddress"' + "type=" +'"hidden"' + "value=" +'"Nil"' +" autofocus />"+
        "<input id=" +'"registerpassword"' + "type=" +'"hidden"' + "value=" +'"Nil"' +" autofocus />"+
        "<input id=" +'"retyperegisterpassword"' + "type=" +'"hidden"' + "value=" +'"Nil"' +" autofocus />";


    }
    else if (userChoice === "Register") {
      document.getElementById("thesignin").style.color = "grey";
      document.getElementById("theregister").style.color = "black";
      var myForm = document.getElementById("theform");
      document.getElementById("theform").innerHTML = "<p class=" + '"title"' + ">Register</p>" +
        "<input id=" +'"newfullname"' +" type=" + '"text"' + "placeholder=" + '"Full Name"' + "autofocus/>" +
        "<input id=" +'"newemailaddress"' +" type=" + '"email"' + "placeholder=" + '"Email"' + "autofocus/>" +
        "<input id=" +'"registerpassword"' +"type=" + '"password"' + "placeholder=" + '"Password"'  + " required>" +
        "<input id=" +'"retyperegisterpassword"' +" type=" + '"password"' + "placeholder=" + '"Retype Password"' + " />" +
        "<button>" +
        "<span class=" + '"state"' + "><b>Register</b></span>" +
        "</button> " +
        "<br></br>"+
        "<br></br>"+
        "<b><span id=" +'"registrationmessage"' +"></span></b>"+

        "<input id=" +'"id1"' + "type=" +'"hidden"' + "value=" +'"Nil"' +" autofocus />"+
        "<input id=" +'"id2"' + "type=" +'"hidden"' + "value=" +'"Nil"' +" autofocus />";
        

        myForm.onsubmit =  handleRegister;
       
        


    }

  })

  return (


    <Grid container>

      <Grid item xs={6} align="center" onClick={() => setChoice(userChoice = "SignIn")} id="thesignin" >
        <Avatar className={classes.avatar} >
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5"  >
          Sign In
        </Typography>
      </Grid>




      <Grid Item xs={6} align="center" onClick={() => setChoice(userChoice = "Register")} id="theregister">
        <Avatar className={classes.avatar}>
          <CreateIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
      </Grid>


    </Grid>





  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
    
  },
  image: {
    backgroundImage: 'url(https://blog-www.pods.com/wp-content/uploads/2019/04/MG_1_1_New_York_City-1.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    fontFamily: 'Nunito',
    fontStyle: 'italic'

  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),

  },
  title: {

  },
  appbar: {
    background: 'none',
    fontFamily: 'Nunito',
    fontStyle: 'italic'

},
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));



export default function SignInSide() {
  const classes = useStyles();


  const handleLogin = (e) => {
    e.preventDefault()
    //console.log(users.Users.map((account) => account.email))

    var userID = document.getElementById("id1").value;
    console.log(userID)
    var userPassword = document.getElementById("id2").value;
    const email = users.Users.map((account) => account.email);
    const password = users.Users.map((account) => account.password);
    const group = users.Users.map((account) => account.group);
    let number = 1;


    if (userID === "" || userPassword === "") {
      document.getElementById("emailError").innerHTML = "Please input an email.";
      document.getElementById("emailError").style.color = "red";
      document.getElementById("id1").style.borderColor = "red";
      document.getElementById("emailError2").innerHTML = "Please input a password.";
      document.getElementById("emailError2").style.color = "red";
      document.getElementById("id2").style.borderColor = "red";
      return;
    } 

     // check if there is any accounts that are not yet approved
     while(true){

      if(!localStorage.getItem("registereditem"+number) === false){

        var registeredCustomer = localStorage.getItem("registereditem"+number);
        const customerObj = JSON.parse(registeredCustomer);

        if(document.getElementById("id1").value == customerObj["email"] && document.getElementById("id2").value == customerObj["password"]){

          document.getElementById("loginmessage").innerHTML = "Account has not been approved yet";
          document.getElementById("loginmessage").style.color = "red";
          document.getElementById("emailError").innerHTML = "";
          document.getElementById("emailError2").innerHTML = "";
          return;
        }

        number++;


      }else{
        break;
      }
    }


    for (let i = 0; i < email.length; i++) {
      if (email[i] === userID && password[i] === userPassword) {
        console.log("login SUCESS");
        document.getElementById("id1").style.borderColor = "black";
        document.getElementById("id2").style.borderColor = "black";
        document.getElementById("emailError").innerHTML = "";
        document.getElementById("emailError2").innerHTML = "";
        document.getElementById("loginmessage").innerHTML = "Login SUCCESS";
        document.getElementById("loginmessage").style.color = "green";
        if (group[i] === "customer") {
          window.setTimeout(function () {
            window.location.href = 'http://localhost:3001/userdashboard';
          }, 500);
        }

        if(group[i] ==="admin"){
          window.setTimeout(function () {
            window.location.href = 'http://localhost:3001/admindashboard';
          }, 500);
        }

        break;
      } 

     else if(userID === "Nil" && userPassword === "Nil"){
        console.log("In registration tab");
        return;
      
      }
      
      
      else {

        document.getElementById("loginmessage").innerHTML = "Login Failed. Email Address or Password is Invalid. Please Type again.";
        document.getElementById("loginmessage").style.color = "red";
        document.getElementById("emailError").innerHTML = "";
        document.getElementById("emailError2").innerHTML = "";
        console.log("login FAIL");

      }
    }

  }



  
  return ( 
      
      <Grid container component="main" className={classes.root} >
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square style={{ background: 'linear-gradient(#ff6f69,#ffcc5c)' }}>
        <div className={classes.paper}>
          <h2>Optimum DigiBank</h2>
          <Changetofaint />

          
          <form onSubmit = {handleLogin} className={classes.form} noValidate id="theform">
            <p className="title">Log in</p>
            <input id="id1" type="text" placeholder="Email" autoFocus />
            <span id="emailError"></span>
            <input id="id2" type="password" placeholder="Password" />
            <span id="emailError2"></span>
            <br/>
            <br/>
            <a href="/forgetpassword">Forgot your password?</a>
            <button>
              <span class="state"><b>Log in</b></span>

            </button>
            <br></br>
            <b><span id="registrationmessage"></span></b>
            <br></br>
            <br></br>
            <br></br>
            
           
            <b><span id="loginmessage"></span></b>

          
          </form>
          </div>
          
        
        
      </Grid>
    </Grid>
  );
  

}