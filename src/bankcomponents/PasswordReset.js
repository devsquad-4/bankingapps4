
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import CreateIcon from '@material-ui/icons/Create';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import {useEffect, useState } from "react";
import data from "./users"; 



const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
    backgroundColor: 'orange',
  },
  image: {
    backgroundImage: 'url(https://blog-www.pods.com/wp-content/uploads/2019/04/MG_1_1_New_York_City-1.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    
    
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
   
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));



function changethepassword(e){

  e.preventDefault();
  // declare important variables
  var doesuserexist = true;
  var fullname;
  var userpassword;
  var userID;
  var useremail;
  var usergroup;

  

  // retrieve the values inside the input fields
   var usernameinput = document.getElementById("theusername").value;
   var newpassword = document.getElementById("newpassword").value;
   var retypepassword = document.getElementById("retypepassword").value;

   // create array of json file
   const username = data.Users.map((account) => account.fullname);
   const password = data.Users.map((account) => account.password);
   const theids = data.Users.map((account) => account.id);
   const theemails = data.Users.map((account) => account.email);
   const groups = data.Users.map((account) => account.group);

   // password format
   var validPasswordRegex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/;

   
  // check if any fields are blank
  if(newpassword === "" || retypepassword === "" || usernameinput === "" ){

    document.getElementById("errormessage").style.color = "red";
    document.getElementById("errormessage").innerHTML = "Please fill up all the fields before submitting";
    return;

  }

  // check condition if retype password is same as newpassword. If not the same, not allowed
  if(newpassword !== retypepassword){
    document.getElementById("errormessage").style.color = "red";
    document.getElementById("errormessage").innerHTML = "Password and Retype password are not the same. Please Type again.";
    document.getElementById("newpassword").value = "";
    document.getElementById("retypepassword").value = "";
    return;
  }


  // check if new password matches required format
  if(!newpassword.match(validPasswordRegex)){
    document.getElementById("errormessage").innerHTML = "Password must be of length 6, contains a number, and a symbol";
    document.getElementById("newpassword").value = "";
    document.getElementById("retypepassword").value = "";
    document.getElementById("errormessage").style.color = "red";
    return;

  }

  for(let i = 0; i<username.length; i++){
    // check condition see if username exist. If does not exist, not allowed
    if(theemails[i] === usernameinput){
      fullname = username[i];
      userpassword = password[i];
      userID = theids[i];
      useremail = theemails[i];
      usergroup = groups[i];
      doesuserexist = true;
      break;
    }
    else{
      doesuserexist = false;
      
    }
  }

  console.log(doesuserexist);

  if(doesuserexist === true){

    // check condition if new password is same as old password. If yes, not allowed
    if(userpassword === newpassword ){
      document.getElementById("errormessage").style.color = "red";
      document.getElementById("errormessage").innerHTML = "This password is already being used. Please enter a new password";
      document.getElementById("newpassword").value = "";
      document.getElementById("retypepassword").value = "";
      return;

    }else{
       // password successfully change
       document.getElementById("errormessage").innerHTML = "Password Successfully Changed";
       document.getElementById("errormessage").style.color = "green";

       for(let i = 0; i<username.length; i++){
        // check condition see if username exist. If does not exist, not allowed
        if(theemails[i] === usernameinput){
          localStorage.setItem('Password'+i, document.getElementById("newpassword").value.toString());
          
          // change the password in the JSON file also
          const axios = require('axios');

          axios.put('http://localhost:3000/users/'+userID +'/', {
            "id": userID,
            "fullname": fullname,
            "email": useremail,
            "password": document.getElementById("newpassword").value.toString(),
            "group": usergroup
          }).then(resp => {

              console.log(resp.data);
          }).catch(error => {

              console.log(error);
          });  

          // re-direct to the password changed UI 
          window.setTimeout(function() {
            window.location.href = 'http://localhost:3001/passwordchanged';
          }, 500);
          break;
        }
       
      }
    }
  }else{
      document.getElementById("errormessage").innerHTML = "Sorry the Email does not exist. Please Type again";
      document.getElementById("errormessage").style.color = "red";
      document.getElementById("newpassword").value = '';
      document.getElementById("theusername").value = '';
      document.getElementById("retypepassword").value = '';
      return;
  }

 

    
}



export default function PasswordReset() {
  const classes = useStyles();

    

  return (
    <Grid container component="main" className={classes.root} >
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square style={{background: 'linear-gradient(#ff6f69,#ffcc5c)'}}>
     <div className={classes.paper}>
        <h2>Optimum DigiBank</h2>
        

        <form className={classes.form} noValidate id="theform">
        <p class="title">Reset Password</p>
        <input type="text" placeholder="Email" id="theusername"/>
        <input type="password" placeholder="New Password" id="newpassword"/>
        <input type="password" placeholder="Re-Type New Password" id="retypepassword" /> 
        <button onClick ={changethepassword}>
         <span class="state"><b>Submit</b></span>
        </button>
        <br></br>
        </form>
        <br></br>
        <b><span id="errormessage"></span></b>

        </div>
      </Grid>
    </Grid>
  );
}
