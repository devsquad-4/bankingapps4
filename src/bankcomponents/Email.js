import EmailIcon from '@material-ui/icons/Email';
import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';



function Removebackground(){
    document.body.style.background = "url() 0% fixed";
}

const useStyles = makeStyles((theme) => ({

    avatar: {
      margin: theme.spacing(25,84),
      backgroundColor: theme.palette.secondary.main,
      position: 'absolute',
      left: 0,
      right: 0,
      height: 200,
      width: 200,
   
    },

    emailsent:{
        margin: theme.spacing(50,90.8),
        color: 'black',
        position: 'absolute',
        left: 0,
        right: 0,
       

    },

    wehavesentyouemail:{
        margin: theme.spacing(51,68),
        color: 'black',
        position: 'absolute',
        left: 0,
        right: 0,
    },

    tryagain:{
        margin: theme.spacing(53,71),
        color: 'black',
        position: 'absolute',
        left: 0,
        right: 0,
    },

    icon:{
        height: 170,
        width: 170,
    }
    
   
 
  }));

export default function Email(){
    const classes = useStyles();
    <Removebackground />

    return(
 
        <body id="emailcontainer" style={{background: 'linear-gradient(#ff6f69,#ffcc5c)'}}>
        <Avatar className={classes.avatar} >
            <EmailIcon className={classes.icon} />
        </Avatar>
        <br></br>
        <span className={classes.emailsent}><b>Email Sent</b></span> 
        <br></br>

        <span className={classes.wehavesentyouemail}>We have sent you an email with a link to reset your password </span>
        <span className={classes.tryagain}>Didnt get the email or not your email address?<Link href="/forgetpassword">try again</Link></span>
        </body>
    );
}