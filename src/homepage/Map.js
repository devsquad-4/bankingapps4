import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import mapboxgl from 'mapbox-gl'
import ReactMapGL, { Marker } from 'react-map-gl'
import { makeStyles } from '@material-ui/core'
import { CallMissedSharp, FormatAlignCenter } from '@material-ui/icons';
import MapGL, {
    Popup,
    NavigationControl,
    FullscreenControl,
    ScaleControl,
    GeolocateControl
} from 'react-map-gl';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    colorTitle: {
        color: '#E77607'
    },
    container: {
        width: '50%',

    },
    text: {
        fontFamily: 'Nunito',
        color: '#F2F0F7',
        textAlign: 'center'
    },
    map: {
        flexGrow: '1'
    }

}))
const navStyle = {
    top: 0,
    left: 0,
    padding: '10px',
    background: '#fff',
    borderTop: '4px'
};
export default function Mapp() {
    const classes = useStyles();

    let [viewport, setViewport] = useState({
        latitude: 1.3334864928615515,
        longitude: 103.96551908012563,
        zoom: 16,
        width: '50%',
        height: '40vh'
    });

    return (

        <div className={classes.root}>
            <div className={classes.container}>
                <h1 className={classes.text}>Contact <span className={classes.colorTitle}>Us</span></h1>
                <h5 className={classes.text}>We are located at</h5>
                <h5 className={classes.text}> 1 Changi Business Park Crescent,</h5>
                <h5 className={classes.text}> Plaza8 @ CBP, #03-09 to #03-12,</h5>
                <h5 className={classes.text}> Podium B, 486025</h5>



            </div>

            <div className={classes.map}>
                <ReactMapGL mapStyle={'mapbox://styles/mapbox/streets-v11'} mapboxApiAccessToken={'pk.eyJ1IjoidmFuZG9ncmUiLCJhIjoiY2txaGtwbGgxMDMxbjJ2cGRkbzYzc25lcyJ9.fyX-80JM5nSnvR_xT1Vlbw'}
                    {...viewport}
                    onViewportChange={(newView) => setViewport(newView)}>
                    <NavigationControl style={navStyle} />
                    <Marker latitude={1.333534759547231} longitude={103.9654708003639}>
                        <img src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/map-marker-512.png" width={50} height={50} />
                    </Marker>

                </ReactMapGL>
            </div>



        </div>
    );
}
