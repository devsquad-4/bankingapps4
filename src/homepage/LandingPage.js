import React from 'react'
import { makeStyles } from '@material-ui/core'
import { AppBar, IconButton } from '@material-ui/core'
import SortIcon from '@material-ui/icons/Sort'
import { CssBaseline } from '@material-ui/core'
import Header from './component/Header'
import AboutUs from './component/AboutUs'
import Mapp from './Map'

const useStyles = makeStyles((theme) => ({
    root: {
        minHeight: '100vh',
        backgroundImage: `url('https://cdn2.hubspot.net/hubfs/53/business-plan-1.jpg')`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundColor: '#393741',
        backgroundBlendMode: 'overlay'
    }
}))

export default function LandingPage() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <CssBaseline />
            <Header />
            <Mapp/>
            <AboutUs/>
        </div>
    )
}
