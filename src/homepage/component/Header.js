import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core'
import { AppBar, IconButton, Toolbar, Collapse } from '@material-ui/core';
import SortIcon from '@material-ui/icons/Sort'
import CssBaseline from '@material-ui/core/CssBaseline';
import { Button } from '@material-ui/core';
import { fontFamily } from '@material-ui/system';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh'
    },
    appbar: {
        background: 'none',
        fontFamily: 'Nunito',
        fontStyle: 'italic'

    },
    appbarWrapper: {
        width: '80%',
        margin: '0 auto'
    },
    appbarTitle: {
        flexGrow: '1'
    },
    colorTitle: {
        color: '#E77607'
    },
    button: {
        width: '10%',
        backgroundColor: '#C3720C'

    },
    title: {
        fontSize: '3rem',
        fontFamily: 'Nunito',
        fontWeight: 'bold',
        color: '#F2F0F7'
    },
    container: {
        textAlign: 'center',
        alignItems: 'center',

    },
    text: {
        fontFamily: 'Nunito',
        color: '#F2F0F7'
    },
    joinToday: {
        color: '#F2F0F7',
        display: 'flex',
        justifyContent: 'center',
        position: 'static',
        backgroundColor: '#C3720C',

    },
    containJoin:{
        display:'flex',
        justifyContent: 'center'
    }

}))

const gotoLogin = (e) => {
    window.setTimeout(function () {
        window.location.href = 'http://localhost:3001/loginpage';
      }, 500);
}

const gotoRegister = (e) => {
    window.setTimeout(function () {
        window.location.href = 'http://localhost:3001/loginpage';
      }, 500);
}

export default function Header() {
    const classes = useStyles();
    const [checked, setChecked] = useState(false);
    useEffect(() => {
        setChecked(true);
    }, []);
    return (
        <div className={classes.root}>
            <AppBar className={classes.appbar} elevation={0}>
                <Toolbar className={classes.appbarWrapper}>
                    <h1 className={classes.appbarTitle}>Optimum <span className={classes.colorTitle}>DigiBank</span> </h1>
                    <Button variant="contained" color="secondary" className={classes.button} onClick={gotoLogin}>
                        Log in
                    </Button>
                </Toolbar>
            </AppBar>

            <Collapse in={checked}
                {...(checked ? { timeout: 1000 } : {})}
                collapsedHeight={50}>
                <div className={classes.container}>
                    <h1 className={classes.title}>Choose the right bank today</h1>
                    <h3 className={classes.text}>We craft the best possibilies for you</h3>
                    <h3 className={classes.text}>Optimum Digital Bank prioritises in optimising your experiences</h3>
                    <h3 className={classes.text}>We will be the best opportunity in your finger tips</h3>


                </div>
                <div className={classes.containJoin}>
                    <Button variant="contained" color="secondary" className={classes.joinToday} onClick={gotoRegister} >
                        Join Today
                    </Button>
                </div>

            </Collapse>
        </div>

    )
}